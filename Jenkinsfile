final appName = 'wallet-ui'
final nodeLabel = "${appName}-${env.BRANCH_NAME}-${env.BUILD_NUMBER}"

podTemplate(label: nodeLabel,
    containers: [
        containerTemplate(
            name: 'jnlp',
            image: 'huangminghuang/jnlp-slave-docker',
            args:  '${computer.jnlpmac} ${computer.name}'
        ),
        containerTemplate(
            name: 'gcloud',
            image: 'google/cloud-sdk:178.0.0-alpine',
            ttyEnabled: true,
            command: 'cat'
        ),
        containerTemplate(
            name: 'kubectl',
            image: 'lachlanevenson/k8s-kubectl:v1.7.10',
            ttyEnabled: true,
            command: 'cat'
        ),
    ],
    volumes: [
        hostPathVolume(
            hostPath: '/var/run/docker.sock',
            mountPath: '/var/run/docker.sock'
        ),
        hostPathVolume(
            hostPath: '/usr/bin/docker',
            mountPath: '/usr/bin/docker'
        )
    ]
)
{
    node (nodeLabel) {
        final scmVars = checkout scm
        def imageTags = [
                "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}:${scmVars.GIT_COMMIT}",
                "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}:${env.BRANCH_NAME}",
                "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
            ]

        stage('Build') {
            echo 'Building docker image...'
            docker.build("${appName}:${scmVars.GIT_COMMIT}")
        }

        stage('Test') {
            echo 'Running tests...'
            sh("docker run --rm ${appName}:${scmVars.GIT_COMMIT} npm test")
        }

        stage('Push') {
            container('gcloud') {
                echo 'Push image to GCR'
                imageTags.each {
                    sh("docker tag ${appName}:${scmVars.GIT_COMMIT} ${it}")
                    sh("gcloud docker -- push ${it}")
                }
            }
        }

        stage('Deploy') {
            container('kubectl') {
                def deployNamespace = ""

                switch (env.BRANCH_NAME) {
                    case ["master"]:
                        def lines = sh(script: "kubectl get ns staging 2>/dev/null || true", returnStdout: true)
                        if (lines?.trim()) {
                            deployNamespace = "staging"
                        }
                        break
                    default:
                        echo("Skipping deploy. Not master.")
                        // TODO: do something different. deploy to a different or unique namespace? only if tagged?
                }

                if (deployNamespace != "") {
                    container('kubectl') {
                        echo("creating kubeconfig")
                        sh("KUBECONFIG=./kubeconfig kubectl config set-cluster mc --server=https://kubernetes.default --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")
                        sh("KUBECONFIG=./kubeconfig kubectl config set-context mc --cluster=mc")
                        sh("KUBECONFIG=./kubeconfig kubectl config set-credentials user --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)")
                        sh("KUBECONFIG=./kubeconfig kubectl config set-context mc --user=user")
                        sh("KUBECONFIG=./kubeconfig kubectl config use-context mc")
                    }

                    container('kubectl') {
                        echo("deploying to ${deployNamespace}")

                        // deployment, ingress, service
                        sh("kubectl --namespace=${deployNamespace} apply -f ./k8s/deployment-with-istio.yaml")
                        sh("kubectl --namespace=${deployNamespace} apply -f ./k8s/ingress.yaml")
                        sh("kubectl --namespace=${deployNamespace} apply -f ./k8s/service.yaml")

                        // until istio supports CNI, the certificate must be
                        // deployed in the istio-system namspace
                        sh("kubectl --namespace=istio-system apply -f ./k8s/certificate.yaml")
                    }
                }
            }
        }

        //stage('IntegrationTest') {
            //get the integration script file from infrastructure
            //checkout([$class: 'GitSCM', branches: [[name: '*/rysanekm-dev']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'infrastructure-src']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'BitBucketCheckout_Cred_ID', url: 'https://bitbucket.org/objectcomputing/infrastructure.git']]])
            //#include it
            //infrastructureUtils = load ('infrastructure-src/utils/integration-test.groovy');
            //if ( infrastructureUtils.RunEndToEndTest ( env.CLUSTER_NAME ) == false ) {
                //currentBuild.result = 'FAILURE';
            //};
        //}
    }
}

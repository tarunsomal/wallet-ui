import React from 'react';

const Success = () => (
  <div className="card border-success mb-3">
    <div className="card-body text-success">
      <h3 className="card-title display-3">
        <i className="fa fa-check-circle-o mr-2" aria-hidden="true"></i>
        Success
      </h3>
      <p className="card-text">Your transaction was successful! You can check your order status for updates.</p>
    </div>
  </div>
);

export default Success;

import React from 'react';

function Indicators(cards) {
  return Object.keys(cards).map((key) => {
    const active = key == 0 ? 'active': '';
    return (<li key={key} data-target="#indicators" className={active} data-slide-to={key}></li>)
  })
}

function Items(cards) {
  return Object.keys(cards).map((key) => {
    const active = key == 0 ? 'active': '';
    const card = cards[key];
    const itemClass = `carousel-item ${active}`;
    return (
      <div key={key} className={itemClass}>
        <img className="d-block w-100 h-25" src={card.icon} alt={card.name} />
      </div>
    )
  })
}

const Carousel = (props) => {
  return (
    <div id="indicators" className="carousel slide" data-ride="carousel" data-interval="false">
      <ol className="carousel-indicators"> { Indicators(props.cards) } </ol>
      <div className="carousel-inner">
      { Items(props.cards) }
      </div>
      <a className="carousel-control-prev" href="#indicators" role="button" data-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a className="carousel-control-next" href="#indicators" role="button" data-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
    </div>
  )
};

export default Carousel;

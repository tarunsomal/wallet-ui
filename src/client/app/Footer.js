import React from 'react';

const Footer = () => (
    <footer className="py-5 bg-light">
      <div className="container">
        <p className="m-0 text-center text-dark">Copyright © Christmas Commerce 2017</p>
      </div>
    </footer>
);

export default Footer;

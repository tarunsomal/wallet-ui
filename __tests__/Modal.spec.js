import React from 'react';
import axios from 'axios';
import Modal from '../src/client/app/Modal';
import renderer from 'react-test-renderer';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon'

Enzyme.configure({ adapter: new Adapter() });

jest.mock('axios', () => ({
  get: jest.fn(() => Promise.resolve({ data: 3 }))
}))

describe('Modal', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Modal />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should have an open but to be clicked', () => {
    const wrapper = mount(<Modal />);
    wrapper.find('#btnOpen').simulate('click');
  });

  it('should call onClickClose() when clicked', () => {
    const wrapper = mount(<Modal />);
    wrapper.find('#btnClose').simulate('click');
    expect(wrapper.state().success).toBe(false);
  });
});

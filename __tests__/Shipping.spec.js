import React from 'react';
import Shipping from '../src/client/app/Shipping';
import renderer from 'react-test-renderer';

describe('Nav', () => {
  it('renders correctly', () => {
    const obj = {
      first_name: 'First',
      last_name: 'Last',
      street_address: '1235 Address',
      state_province: 'MO',
      postal_code: '123456'
    }

    const tree = renderer.create(
      <Shipping wallet={obj} />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

import React from 'react';
import Reviews from '../src/client/app/Reviews';
import renderer from 'react-test-renderer';

describe('Nav', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Reviews />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
